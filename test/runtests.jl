module Birthdaze
include("../src/Birthdaze.jl")
end
using Test

@test Birthdaze.calc_bday_pair(1) == 0.0
@test Birthdaze.calc_bday_pair(12) == 0.1782
@test Birthdaze.calc_bday_pair(23) == 0.5154
@test Birthdaze.calc_bday_pair(73) == 0.9994
