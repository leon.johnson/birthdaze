# The Birthday counter-intuitive thing

"""
    *(x)

Return percentage that there will be a pair of people with the same birthday.
"""
function calc_bday_pair(x)
days = 365
possible_pairs = (x * x - 1)/2
chance_unique_pair = (days - 1)/days
chance_n_unique_pair = chance_unique_pair^possible_pairs
chance_match = 1 - chance_n_unique_pair
magic =  ceil(chance_match, sigdigits=4)
return magic
end
